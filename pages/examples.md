// { % set _global_phone = global.phone % }
// { % set global.phone = "8-800-000-00-00" % }

/url /examples
/simplepage true
/title Примеры блоков
/header Примеры Блоков

/+blocks
/template benefits
/image customer-story-1.jpg
/price от 25 000 <i class="fa fa-rub"></i>/мес
## Наши супер услуги
### Получаете
* Пункт 1
* Пункт 2
### Дополнительно

<i class="fa fa-rub"></i>
/-

/+blocks
/template benefits2
/image uslugi02.jpg
/price 1000 <i class="fa fa-rub"></i>/мес
## Наши супер услуги длинные
### Получаете
* Пункт 1
* Пункт 2
### Дополнительно
/-

/+blocks
/template uni-zap
/theme zap1-theme
/background customer-story-1.jpg

/+items
/cols 8
/-

/+items
/cols 4
/title Заголовок
Не множеством картин старинных мастеров <br />
Украсить я всегда желал свою обитель, <br />
Чтоб суеверно им дивился посетитель, <br />
Внимая важному сужденью знатоков. <br />

В простом углу моем, средь медленных трудов, <br />
Одной картины я желал быть вечно зритель, <br />
Одной: чтоб на меня с холста, как с облаков, <br />
Пречистая и наш божественный спаситель - <br />

Она с величием, он с разумом в очах - <br />
Взирали, кроткие, во славе и в лучах, <br />
Одни, без ангелов, под пальмою Сиона. <br />

Исполнились мои желания. Творец <br />
Тебя мне ниспослал, тебя, моя Мадонна, <br />
Чистейшей прелести чистейший образец. <br />
/-

/-

/add-blocks examples/content30.md

/add-blocks examples/title.md

//**************Видео**************
/+blocks
/template uni
/theme zap2-theme

/+items
/cols 5
<iframe class="" width="100%" height="270" src="https://www.youtube.com/embed/04tffSuqFpk?rel=0&amp;wmode=transparent&amp;showinfo=0&amp;theme=light&amp;autohide=1" frameborder="0" allowfullscreen=""></iframe>
/-

/+items
/cols 1
/-

/+items
/cols 6
/title Видео о том как я был большой
/button Подробнее
/buttonlink /
Не множеством картин старинных мастеров <br />
Украсить я всегда желал свою обитель, <br />
Чтоб суеверно им дивился посетитель, <br />
Внимая важному сужденью знатоков. <br />
/-

/-

/add-blocks examples/list.md

/add-blocks examples/uni.md

/add-blocks examples/content14.md

/add-blocks examples/price1.md





/+blocks

/template uni
/header Заголовок блока

/text Дополнительный текст

/+items
/cols 12
/title Заголовок
// rjvvtynfhbq
| h1    |    h2   |      h3 |
|:------|:-------:|--------:|
| 100   | [a][1]  | ![b][2] |
| *foo* | **bar** | ~~baz~~ |

/-

/-

/+blocks
/template table
/header Заголовок таблицы
/load-tables examples/test.xlsx

/-

/+blocks
/template benefits35
/header На сайте оставляют мало заявок?

Разрабатываем на основе статистического анализа запросов новые продающие сайты и целевые страницы (landing page), спроектированные для работы с контекстной рекламой.
Адаптируем существующие сайты до состояния готовности работы с контекстной рекламы.

/image norequest.jpg

/+items
/title Получаем в итоге
высоконверсионный сайт, с которого поступают входящие обращения
/-

/+items
/title Плюс к этому
возможность работать со старым сайтом и не ждать, пока будет готов новый
/-
/-

/+blocks
/template benefits35r
/header Менеджеры пропускают входящие звонки и не перезванивают потенциальным клиентам?

Устанавливаем и запускаем систему IP-телефонии с функцией записи звонка.<br>
Запускаем систему «Скрипты звонков» - документированные алгоритмы телефонных разговоров для менеджеров.<br> Пишем эти алгоритмы.

/image telephone_off_hook.jpg

/+items
/title Получаем в итоге
учет и полный контроль звонков, их запись, интеграция телефонии с CRM. Исключаются потери звонков.
/-

/+items
/title Что приводит к
существенному росту эффективности телефонных переговоров. Больше сделок. <br/> Возможность быстро включать в работу нового менеджера с минимальным опытом.
/-
/-

/+blocks
/template content49
/image factory01.jpg
/header Заголовок
/title Наше производство находитсяв г.Кузнецке Пензенской области
/num 01.
/+items
/icon search
Пилорамма
/-
Производственной мощности фабрики достаточно, чтобы производить практически любую мебель по вашим размерам за 7 дней.<br />

/-

/+blocks
/template content49-2
/image factory01.jpg
//header Заголовок
/title Наше производство находитсяв г.Кузнецке Пензенской области
/num 01.
/+items
// https://fortawesome.github.io/Font-Awesome/icons/
/icon search
Пилорамма
/-
Производственной мощности фабрики достаточно, чтобы производить практически любую мебель по вашим размерам за 7 дней.<br />
/-

