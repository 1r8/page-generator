var gulp = require('gulp');
var fs = require('fs');
var path = require('path'),
    watch = require('gulp-watch');

var yaml = require('js-yaml');
var _ = require('lodash');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');

var vendor_files = yaml.safeLoad(fs.readFileSync('./vendor/vendor.yml', {encoding: 'utf8'}));

var paths = {
	dist: '_dist_',
	tmp: '_tmp_'
};

var production = false;

var less = require('gulp-less');

var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('less', function () {
  var result = gulp.src('./styles/**.less')
    .pipe(sourcemaps.init())
    .pipe(less({
      paths: [ path.join(__dirname, 'styles', 'includes') ]
    }));

    if (!production)
        result = result.pipe(sourcemaps.write());

    result = result
      .pipe(autoprefixer({
          browsers: ['not ie <= 8'],
          cascade: false
      }));
    if (production)
        result = result.pipe(minifyCss({compatibility: 'ie9'}));

    result = result.pipe(gulp.dest('./_dist_/css'));

    return result;
});

gulp.task('styles', ['less'], function() {
});


var fileNamesToJson = require('./gulp-plugins/gulp-file-names-to-json');

gulp.task('copy-vendor', function () {
    return gulp.src(['./vendor/**/*.*'])
        .pipe(gulp.dest(paths.dist));
});

gulp.task('copy-fonts', function () {
    var sources = [];
    _.each(vendor_files.fonts, function(info){
        sources.push('./vendor/' + info.relative);
    });

    return gulp.src(sources)
        .pipe(gulp.dest(paths.dist + '/fonts'));
});


gulp.task('copy-images', function () {
    return gulp.src(['./images/**/*.*'])
        .pipe(gulp.dest(paths.dist + '/img/'));
});

gulp.task('copy-files', function () {
    return gulp.src(['./files/**/*.*'])
        .pipe(gulp.dest(paths.dist + '/files/'));
});

var renderPageMd = require('./gulp-plugins/gulp-render-page-md');

gulp.task('pages',['riot'], function () {
    var global = yaml.safeLoad(fs.readFileSync('./elements/global.yml', {encoding: 'utf8'}));
    var vendor = yaml.safeLoad(fs.readFileSync('./vendor/vendor.yml', {encoding: 'utf8'}));
    var main_menu = yaml.safeLoad(fs.readFileSync('./elements/menu-main.yml', {encoding: 'utf8'}));
    var footer_menu = yaml.safeLoad(fs.readFileSync('./elements/menu-footer.yml', {encoding: 'utf8'}));
    /*
     var main_menu2 = yaml.safeLoad(fs.readFileSync(paths.src + '/data/menu-main2.json', {encoding: 'utf8'}));
     var main_footer = yaml.safeLoad(fs.readFileSync(paths.src + '/data/menu-footer.json', {encoding: 'utf8'}));
     var settings = yaml.safeLoad(fs.readFileSync(paths.src + '/data/settings.json', {encoding: 'utf8'}));
     */
    var riot_files = JSON.parse(fs.readFileSync(paths.tmp + '/riot-js/riot-files.json', {encoding: 'utf8'}));
    return gulp.src(['./pages/*.md'])
        .pipe(renderPageMd({
            pagesPath: './pages',
            templatesPath: './templates/',
            context: {
                __production: production,
                vendor: vendor,
                global: global,
                riot_files: riot_files,
                galleries: [],
                menus: {
                    main: main_menu,
                    footer: footer_menu
                }
            }
        }))
        //.pipe($.print())
        .pipe(gulp.dest(paths.dist))
        ;
});

gulp.task('watch', function() {
    gulp.watch('styles/**/*.*', ['styles']);
    gulp.watch('templates/*.html', ['pages']);
    gulp.watch('images/*.*', ['copy-images', 'pages']);
    gulp.watch('pages/**', ['pages']);
    gulp.watch('sources/riot/*.tag', ['riot']);
});

var webserver = require('gulp-webserver');

gulp.task('webserver', ['watch'], function() {
    gulp.src(['./_dist_'])
        .pipe(webserver({
            port:9080,
            host: 'localhost',
            livereload: true,
            //directoryListing: true,
            open: true
            //fallback: 'index.html'
        }));
});

var runSequence = require('run-sequence');

gulp.task('build', function(callback) {
    runSequence(
        'cleartmp',
        'styles',
        'riot',
        'copy-images',
        'copy-fonts',
        'copy-vendor',
        'copy-files',
        'pages',
        callback);
});

gulp.task('default', ['build'], function(callback) {
  runSequence('webserver',
              callback);
});

gulp.task('serve-prod', function(callback) {
    production = true;
    runSequence(
        'clear',
        'build',
        'webserver',
        callback);
});


var ftp = require( 'vinyl-ftp' );
var gutil = require( 'gulp-util' );

var clean = require('gulp-clean');

gulp.task('cleartmp', function () {
    return gulp.src([paths.tmp], {read: false})
        .pipe(clean());
});

gulp.task('clear', function () {
    return gulp.src([paths.dist, paths.tmp], {read: false})
        .pipe(clean());
});

gulp.task('deploy', function(callback) {
    production = true;
    runSequence('clear',
        'build',
        'deploycdn',
        callback);
});


gulp.task( 'deploycdn', function () {


    var child_process = require('child_process');
    child_process.execSync('node deploy.js');//, {cwd: './'});

} );

var riot = require('gulp-riot');

gulp.task('compile-riot', function() {
    return gulp.src('sources/riot/*.tag')
        .pipe(riot({modular:true}))
        .pipe(gulp.dest('_tmp_/riot-js'));
});

gulp.task('copy-riot-files', ['compile-riot'],  function () {
    var result = gulp.src(['_tmp_/riot-js/*.js']);

    result = result.pipe(gulp.dest(paths.dist + '/js/'));
    return result;
});

gulp.task('riot', ['compile-riot', 'riot-files-to-json', 'copy-riot-files'],  function() {
    var result = gulp.src('sources/riot/*.tag')
        .pipe(riot({modular:true}));
    if (production)
        result = result.pipe(concat('riot-tags.js'));

    result = result.pipe(gulp.dest('_tmp_/riot-js'));
    return result;
});

gulp.task('riot-files-to-json', ['compile-riot'], function () {
    return gulp.src([paths.tmp + '/riot-js/*.js'], {read: false})
        .pipe(fileNamesToJson('riot-files.json', {
            relative: path.join(paths.tmp, 'riot-js')
        }))
        .pipe(gulp.dest(paths.tmp + '/riot-js/'));
});