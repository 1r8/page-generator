'use strict';

// through2 is a thin wrapper around node transform streams
var through = require('through2');
var gutil = require('gulp-util');
var PluginError = gutil.PluginError;
var path = require('path');

var PLUGIN_NAME = 'gulp-file-names-to-json';

// Plugin level function(dealing with files)
function gulpFileNamesToJson(file, opt) {

    if (!file) {
        throw new PluginError(PLUGIN_NAME, 'Missing file option for ' + PLUGIN_NAME);
    }

    opt = opt || {};


    var files = [];

    function bufferContents(file, enc, cb) {
        if (!file.isDirectory()) {
            files.push({
                basename: path.basename(file.path),
                relative: path.relative(opt.relative, file.path).replace(/\\/g, "/")
            });
        }

        cb();
    }

    function endStream(cb) {

        var resultFile = new gutil.File({
            path: file
        });

        resultFile.contents = new Buffer(JSON.stringify(files));
        this.push(resultFile);
        cb();
    }

    return through.obj(bufferContents, endStream);

}

// Exporting the plugin main function
module.exports = gulpFileNamesToJson;