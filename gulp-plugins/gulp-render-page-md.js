'use strict';

// through2 is a thin wrapper around node transform streams
var through = require('through2'),
    gutil = require('gulp-util'),
    moment = require('moment'),
    numeral = require('numeral'),
    crypto = require('crypto'),
    fs = require('fs-extra'),
    _ = require('lodash');


var PluginError = gutil.PluginError;


moment.locale('ru');
numeral.language('ru', require('numeral/languages/ru'));
numeral.language('ru');

var path = require('path');
var swig = require('swig');
swig.setDefaults({ 
    cache: false
});

function removeP(input) { var output = input.replace(/<p>/gm, '').replace(/<\/p>/gm, '').replace(/\r?\n\r?/gm, ''); return output; }
function replaceP(input) { return input.replace(/<p>/gm, '').replace(/<\/p>/gm, "<br/>"); }
function addCssClass(input, tag, cls) { 
    var re = new RegExp("<" + tag + ">", 'gm');
    var output = input.replace(re, '<'+tag + ' class="' + cls + '">'); 
    return output; 
}
swig.setFilter('removeP', removeP);
swig.setFilter('replaceP', replaceP);
swig.setFilter('addCssClass', addCssClass);

swig.setFilter('randomId', function(count) {
    return crypto.randomBytes(count).toString('hex');
});

swig.setFilter('trunc', _.trunc);


swig.setFilter('add2array', function(array, value) {
    array = array || [];
    if (value)
        array.push(value);
    return array;
});


swig.setFilter('setmap', function(map, key, value) {
    var result = map || {};
    if (key && value)
        result[key] = value;
    return result;
});

swig.setFilter('split', function(value, sep) {
    return value.split(sep || ",");
});

swig.setFilter('startsWith', function(str, substr, pos) {
    return str.startsWith(substr, pos);
});

swig.setFilter('union', function(array, array2) {
    return _.union(array || [], array2 || []);
});

function resizeImage(input, rcmd) {

    if (!fs.existsSync(path.join('./images', input)))
        return input;

    //console.log(input);
    //return input;
    var hash = crypto.createHash('sha1');
    hash.setEncoding('hex');
    //crypto.randomBytes(256).toString('base64');

    hash.write(fs.readFileSync(path.join('./images', input)));
    hash.write(rcmd);

// very important! You cannot read from the stream until you have called end()
    hash.end();

// and now you get the resulting hash
    var destFileName = hash.read() + path.extname(input);
    //console.log(input, rcmd, fs.existsSync(path.join('./_dist_/img', destFileName)));

    if (! fs.existsSync(path.join('./_dist_/img', destFileName))) {
        var cmd = ' ' + rcmd + ' ';
        var child_process = require('child_process');
        child_process.execSync('convert ' + path.join('./images', input) + cmd + path.join('./_dist_/img', destFileName), {cwd: './'});
    }

    return destFileName;
};

swig.setFilter('optimizeImage', function(input) {
    return resizeImage(input, '-strip -quality 85 -interlace Plane');
});

swig.setFilter('resizeImage', resizeImage);

function makeGalleryData(sheet, id, production) {
    sheet.settings = sheet.settings || {};
    var all_filters = [];
    var all_filter_values = {};
    var filters_by_row = {};
    _.forEach(sheet.data, function(row, index) {
        if (index === 0 || !_.isNumber(index) || _.isUndefined(row[0]))
            return;

        row.filters = row.filters || [];
        _.forEach(row, function(item, icol){
            if(_.isUndefined(item))
                return;

            var filter = getTableCellImage(sheet.settings.filter, index+1, icol+1);
            if (filter){
                all_filter_values[filter] = _.union( item.split(','), all_filter_values[filter]);
                all_filters.push(filter);
                filters_by_row[index] = filters_by_row[index] || {};
                filters_by_row[index][filter] = item.split(',');
            }
        });
    });

    all_filters = _.uniq(all_filters);

    var resizecmd = sheet.settings.resizecmd || '-size 400x -strip -quality 85 -interlace Plane';

    var result = {
        item_action: sheet.settings.action,
        all_filters: all_filters,
        all_filter_values: all_filter_values,
        items: []
    };

    _.forEach(sheet.data, function(row, index) {
        if (index === 0 || !_.isNumber(index) || _.isUndefined(row[0]))
            return;
        var item = {
            title: row[0],
            shortTitle: row[0],
            image: cdn('/img/' + row[2], production),
            thumb_image: cdn('/img/' + resizeImage(row[2], resizecmd), production),
            filter: filters_by_row[index]
        };
        if (sheet.settings.pricecol)
            item.price = row[sheet.settings.pricecol-1];
        if (sheet.settings.oldpricecol)
            item.oldprice = row[sheet.settings.oldpricecol-1];
        if (sheet.settings.sizecol)
            item.size = row[sheet.settings.sizecol-1];
        if (sheet.settings.accessories)
            item.accessories = row[sheet.settings.accessories-1];
        if (sheet.settings.tabletop)
            item.tabletop = row[sheet.settings.tabletop-1];

        result.items.push(item);
    });

    var data = JSON.stringify(result);
    var hash = crypto.createHash('sha1');
    hash.setEncoding('hex');
    hash.write(data);
    hash.end();

    var gId = hash.read();
    var fn = path.join('./_dist_', 'data', gId + '.json');
    if (! fs.existsSync(fn) ) {
        fs.outputFileSync(fn, data);
    }
    return gId;
}

swig.setFilter('makeGalleryData', makeGalleryData);

function hashName(input, flag) {
    if (!flag)
     return input;

    var fn = path.join('./_dist_', input);

    if (!fs.existsSync(fn))
        return input;

    var hash = crypto.createHash('sha1');
    hash.setEncoding('hex');

    var file = fs.readFileSync(fn);

    hash.write(file);
    hash.end();

    var destFileName = path.dirname(input) + '/' + hash.read() + path.extname(input);
    var newFn = path.join('./_dist_', destFileName);

    if (! fs.existsSync(newFn) ) {
        fs.writeFileSync(newFn, file);
    }

    // and now you get the resulting hash

    return destFileName;

};

swig.setFilter('hashName', hashName);

function cdn(input, flag) {
    if (!flag)
        return input;

    return 'http://165521.selcdn.com/fab24' + input;
};

swig.setFilter('cdn', cdn);


function getTableColIndex(str) {
    var abc = "AZ";
    var size = abc.charCodeAt(1) - abc.charCodeAt(0);
    var colIndex = 0;            
    _.each(str, function(s) {
        colIndex *= size;
        colIndex += s.charCodeAt(0) - abc.charCodeAt(0) + 1;
    });
    return colIndex;
}

function getTableColspan(input, row, col) { 
    var colspan = _.isArray(input) ? input : [input];

    var result = 0;
    _.each(colspan, function(coords) {
        var m = coords.match(/^([A-Z]+)(\d+)=(\d+)/);
        if (m) {
            var colIndex = getTableColIndex(m[1]);
            var rowIndex = parseInt(m[2]);

            if (colIndex == col && rowIndex == row)
                result = parseInt(m[3]);
        }
    });
    
    return result;
}

swig.setFilter('getTableColspan', getTableColspan);


function getTableCellImage(input, row, col) { 
    var images = _.isArray(input) ? input : [input];

    var result;
    _.each(images, function(coords) {
        var m = coords.match(/^([A-Z]+)(\d+):([A-Z]+)(\d+)=(.+)/);
        if (m) {
            var colIndex1 = getTableColIndex(m[1]);
            var rowIndex1 = parseInt(m[2]);
            var colIndex2 = getTableColIndex(m[3]);
            var rowIndex2 = parseInt(m[4]);

            if (colIndex1 <= col && rowIndex1 <= row && colIndex2 >= col && rowIndex2 >= row)
                result = m[5];
        }
    });
    
    return result;
}

swig.setFilter('getTableCellImage', getTableCellImage);
swig.setFilter('getTableCellInfoRange', getTableCellImage);

var formatTableCell = function (input, params) {
    if (_.isNumber(input)) {
        if (params && params[0] == 'D')
            return moment.unix((input - 25569) * 86400).format(params.substring(1) || 'DD MMMM YYYY');
        else
            return numeral(input).format(params || '0,0.00');
    }

    if (_.isDate(input))
        return moment(input).format(params || 'MMMM DD YYYY, h:mm:ss');

    //console.log(this);
    return input;
};

swig.setFilter('formatTableCell', formatTableCell);

var markdownFilter = function (input) {
    var result = converter.makeHtml(input).replace(/<p>/gm, '').replace(/<\/p>/gm, "<br/>");
    if (result.endsWith("<br/>")) {
        result = result.substring(0, result.length - 5);
    }
    return result;
};

swig.setFilter('markdown', markdownFilter);


var fs = require('fs-extra');
var showdown  = require('showdown'),
    converter = new showdown.Converter();
    converter.setOption('tables', true);
    converter.setOption('strikethrough', true);

var _ = require('lodash');

var PLUGIN_NAME = 'gulp-render-page-md';

var yaml = require('js-yaml');

var parseXlsx = function(fn) {
    

    var xlsx = require('node-xlsx');
    var tables =  xlsx.parse(fn);

    _.each(tables, function(table) 
    { 
        var settingsFlag = false;
        table.settings = {};
        var data = [];
        _.each(table.data, function(row) {
            if (settingsFlag) {
                if (!row[0]) {
                    // комментарий
                    return;
                }

                var values = _.slice(row, 1);

                if (values.length > 1)
                    table.settings[row[0]] = values;
                else
                    table.settings[row[0]] = values[0];                                
                return;
            }

            if (row[0] == "/settings") {
                settingsFlag = true;
                return;
            }

            data.push(row);
        });
        //console.log(table.settings);
        table.data = data;
    });

    return tables;
};

var parseSource = function(source, pagePath, swigContext) {
    var page_src = swig.render(source, { locals: swigContext });
    page_src = page_src.split(/\r?\n/);
    var root_scope = {};
    var scope = root_scope;
    var scope_stack = [];
    var text_content = "";

    var addContent = function() {
        scope.content = scope.content || "";
        scope.content += converter.makeHtml(text_content);
        text_content = "";
    };

    var setElemFlag= false;
    var elemName = "$elem";
    var elemContent = "";
    var elemType = "";
    _.each(page_src, function(line) {
        var m;

        if (line.startsWith("//"))
            return;

        if (setElemFlag) {
            m = line.match(/^\/}\s*/);
            if (m) {
                if (elemType === 'md')
                  elemContent = converter.makeHtml(elemContent);

                if (elemType === 'yml')
                    elemContent = yaml.safeLoad(elemContent);

                scope[elemName] = elemContent;
                elemContent= "";
                setElemFlag = false;
            } else
                elemContent += line + "\n";
            return;
        }

        m = line.match(/^\/([\w\d]+){([\w\d]*)\s*/);
        if (m) {
            elemName = m[1];
            elemType = m[2];
            setElemFlag = true;
            return;
        }

        m = line.match(/^\/-\s*/);
        if (m) {
            addContent();
            scope = scope_stack[scope_stack.length - 1] || root_scope;
            scope_stack.pop();
            return;
        }
        m = line.match(/^\/\+([\w\d]+)\s*/);
        if (m) {
            addContent();
            scope_stack.push(scope);
            var name = m[1];
            scope[name] = scope[name] || [];
            var ns = {};
            scope[name].push(ns);
            scope = ns;
            return;
        }

        m = line.match(/^\/add-([\w\d]+)\s+(.*)/);
        if (m) {
            var name = m[1];
            scope[name] = scope[name] || [];
            var fn = path.join(pagePath, m[2]);

            var ns = {};

            if (path.extname(fn) === '.md') {
                ns = parseSource(fs.readFileSync(fn, "utf8"), pagePath, swigContext);
            }

            if (path.extname(fn) === '.xlsx') {
                ns = parseXlsx(fn);                 
            }
            scope[name].push(ns);
            return;
        }

        m = line.match(/^\/load-([\w\d]+)\s+(.*)/);
        if (m) {
            var name = m[1];
            scope[name] = scope[name] || [];
            var fn = path.join(pagePath, m[2]);

            var ns = {};

            if (path.extname(fn) === '.md') {
                ns = parseSource(fs.readFileSync(fn, "utf8"), pagePath);
            }

            if (path.extname(fn) === '.xlsx') {
                ns = parseXlsx(fn); 
            }
            scope[name] = ns;
            return;
        }        

        m = line.match(/^\/([\w\d]+)\s(.*)/);
        if (m) {
            var name = m[1];
            scope[name] = m[2];
            return;
        }

        m = line.match(/^\/=([\w\d]+)/);
        if (m) {
            addContent();
            scope_stack.push(scope);
            var name = m[1];
            scope[name] = scope[name] || {};
            scope = scope[name];
            return;
        }

        text_content += line + "\n";
    });

    addContent();

    return root_scope;
};

// Plugin level function(dealing with files)
function gulpRenderPage(opt) {

    opt = opt || {
        templatesPath: "",
        context: {}
    };

    function bufferContents(file, enc, cb) {
        if (file.isStream()) {
            this.emit('error', new PluginError(PLUGIN_NAME, 'Streams are not supported!'));
            return cb();
        }

        if (file.isBuffer()) {
            var page_src = file.contents.toString();
            //page_src = swig.render(page_src, { locals: opt.context });

            var page = parseSource(page_src, opt.pagesPath, opt.context);//yaml.safeLoad(swig.render(page_src, { locals: opt.context }));

            page = _.merge(page, opt.context);

            var templatePath = opt.templatesPath + "page.swig.html";

            if (page.template) {
                templatePath = opt.templatesPath + "page-" + page.template + ".swig.html";
            }

            var tpl = swig.compileFile(templatePath);

            var path = "index.html";
            var pageurl = page.url;

            if (pageurl.substring(0, 1) == '/') {
                pageurl = pageurl.substring(1);
            }

            if (pageurl && pageurl != "") {
                path = pageurl + "/index.html";
            }

            var resultFile = new gutil.File({
                path: path
            });

            page.emptyMap = {};
            page.emptyArray = [];

            resultFile.contents = new Buffer(tpl(page));
            this.push(resultFile);
        }

        cb();
    }

    return through.obj(bufferContents);

}

// Exporting the plugin main function
module.exports = gulpRenderPage;